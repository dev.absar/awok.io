
(function(){

	angular
		.module('devAwok', ['ngRoute'])
		.config([ '$routeProvider', '$locationProvider', config ])
		.controller('CtrlDevAwok', ['$scope', '$rootScope', '$http' , ctrlFunction ])
		.controller('CtrlModules', ['$scope', '$rootScope','$http' , ctrlFunction_modules ])
		
		function config( $routeProvider, $locationProvider ){
			$routeProvider
				.when('/', {
					templateUrl : 'views/start.html',
					controller : 'CtrlDevAwok'
				})
				.when('/layout',{
					templateUrl : 'views/layout.html',
					controller : 'CtrlDevAwok'
				})
				.when('/layout_gutter',{
					templateUrl : 'views/layout-gutter.html',
					controller : 'CtrlDevAwok'
				})
				.when('/layout_form',{
					templateUrl : 'views/layout-forms.html',
					controller : 'CtrlDevAwok'
				})
				.when('/layout_template',{
					templateUrl : 'views/layout-template.html',
					controller : 'CtrlDevAwok'
				})
				.when('/typography',{
					templateUrl : 'views/css-typography.html',
					controller : 'CtrlDevAwok'
				})
				.when('/tables',{
					templateUrl : 'views/css-table.html',
					controller : 'CtrlDevAwok'
				})
				.when('/inputs',{
					templateUrl : 'views/css-input.html',
					controller : 'CtrlDevAwok'
				})
				.when('/forms',{
					templateUrl : 'views/css-form.html',
					controller : 'CtrlDevAwok'
				})
				.when('/buttons',{
					templateUrl : 'views/css-buttons.html',
					controller : 'CtrlDevAwok'
				})
				.when('/images',{
					templateUrl : 'views/css-images.html',
					controller : 'CtrlDevAwok'
				})
				.when('/helper',{
					templateUrl : 'views/css-helpers.html',
					controller : 'CtrlDevAwok'
				})
				.when('/modules',{
					templateUrl : 'views/modules.html',
					controller : 'CtrlModules'	
				})
				.when('/colors',{
					templateUrl : 'views/colors.html',
					controller : 'CtrlDevAwok'
				})
				.when('/icons',{
					templateUrl : 'views/icons.html',
					controller : 'CtrlDevAwok'
				})
				.when('/animate',{
					templateUrl : 'views/animate.html',
					controller : 'CtrlDevAwok'
				})
				.otherwise({
					templateUrl : 'views/start.html',
					controller : 'CtrlDevAwok'
				});

			$locationProvider.html5Mode(true);

		}


		function ctrlFunction($scope, $rootScope, $http){
			$scope.mainHeading = "Incroyable";
			scrolltop();
			jQueryInit();
		}
		
		function ctrlFunction_modules(){
			jQueryInit();
			scrolltop();
			devAwokInit();
		}
			

})();


