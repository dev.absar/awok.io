// absar //

$(function(){

	window.currentDom;
	window.edit = true;


	// Edit Click
	$('.js_edit').on('click', function(){

		window.edit = true;

		if( $('*').hasClass('edit_dom') ){
			$('.aw_container, .aw_wrapper, .aw_row, .aw_col').removeClass('edit_dom').removeClass('select_dom').removeClass('selected_dom');
		} else {
			$('.aw_container, .aw_wrapper, .aw_row, .aw_col').addClass('edit_dom');
		}
	})

	$('.js_view').on('click', function(){
		$('*').removeClass('edit_dom').removeClass('select_dom').removeClass('selected_dom');
		window.edit = false;
	})

	// Delete Dom
	$('.js_delete').on('click', function(){
		if( $( window.currentDom ).hasClass('aw_container') || $( window.currentDom ).hasClass('aw_wrapper') ){ return; }
		if( $(window.currentDom)[0].tagName == "HTML" ){ return false; }
		$( window.currentDom ).remove();
	})

	$(document).on('keyup', function(e){

		if( $( window.currentDom ).hasClass('aw_container') || $( window.currentDom ).hasClass('aw_wrapper') ){ return; }
		if( $(window.currentDom)[0].tagName == "HTML" ){ return false;}
		if( window.edit == false ){ return false; }
		if( e.which == 46 ){
			$( window.currentDom ).remove();
		}
	})

	

	// Wrapper Append
	$('.js_wrapper').on('click', function(){
		var domWrapper = '<div class="aw_container edit_dom"><div class="aw_wrapper edit_dom"></div></div>'
		$('body').append( domWrapper );
	})

	$('.js_row').on('click', function(){
		var dom = '<div class="aw_row edit_dom"></div>'
		$( window.currentDom ).append( dom );
	})

	$('.js_cl_1').on('click', function(){
		var dom = '<div class="aw_xl_24 aw_l_24 aw_m_24 aw_s_24 aw_col edit_dom"></div>';
		$( window.currentDom ).append( dom );
	})

	$('.js_cl_2').on('click', function(){
		var dom = '<div class="aw_xl_12 aw_l_12 aw_m_12 aw_s_12 aw_col edit_dom">'+
				  '</div><div class="aw_xl_12 aw_l_12 aw_m_12 aw_s_12 aw_col edit_dom"></div>';
		$( window.currentDom ).append( dom );
	})

	$('.js_cl_3').on('click', function(){
		var dom = '<div class="aw_xl_8 aw_l_8 aw_m_8 aw_s_8 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_8 aw_l_8 aw_m_8 aw_s_8 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_8 aw_l_8 aw_m_8 aw_s_8 aw_col edit_dom"></div>';
		$( window.currentDom ).append( dom );
	})

	$('.js_cl_4').on('click', function(){
		var dom = '<div class="aw_xl_6 aw_l_6 aw_m_6 aw_s_6 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_6 aw_l_6 aw_m_6 aw_s_6 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_6 aw_l_6 aw_m_6 aw_s_6 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_6 aw_l_6 aw_m_6 aw_s_6 aw_col edit_dom"></div>';
		$( window.currentDom ).append( dom );
	})

	$('.js_cl_6').on('click', function(){
		var dom = '<div class="aw_xl_4 aw_l_4 aw_m_4 aw_s_4 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_4 aw_l_4 aw_m_4 aw_s_4 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_4 aw_l_4 aw_m_4 aw_s_4 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_4 aw_l_4 aw_m_4 aw_s_4 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_4 aw_l_4 aw_m_4 aw_s_4 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_4 aw_l_4 aw_m_4 aw_s_4 aw_col edit_dom"></div>';
		$( window.currentDom ).append( dom );
	})

	$('.js_cl_8').on('click', function(){
		var dom = '<div class="aw_xl_3 aw_l_3 aw_m_3 aw_s_3 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_3 aw_l_3 aw_m_3 aw_s_3 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_3 aw_l_3 aw_m_3 aw_s_3 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_3 aw_l_3 aw_m_3 aw_s_3 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_3 aw_l_3 aw_m_3 aw_s_3 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_3 aw_l_3 aw_m_3 aw_s_3 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_3 aw_l_3 aw_m_3 aw_s_3 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_3 aw_l_3 aw_m_3 aw_s_3 aw_col edit_dom"></div>';
		$( window.currentDom ).append( dom );
	})

	$('.js_cl_12').on('click', function(){
		var dom = '<div class="aw_xl_2 aw_l_2 aw_m_2 aw_s_2 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_2 aw_l_2 aw_m_2 aw_s_2 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_2 aw_l_2 aw_m_2 aw_s_2 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_2 aw_l_2 aw_m_2 aw_s_2 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_2 aw_l_2 aw_m_2 aw_s_2 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_2 aw_l_2 aw_m_2 aw_s_2 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_2 aw_l_2 aw_m_2 aw_s_2 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_2 aw_l_2 aw_m_2 aw_s_2 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_2 aw_l_2 aw_m_2 aw_s_2 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_2 aw_l_2 aw_m_2 aw_s_2 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_2 aw_l_2 aw_m_2 aw_s_2 aw_col edit_dom"></div>'+
				  '<div class="aw_xl_2 aw_l_2 aw_m_2 aw_s_2 aw_col edit_dom"></div>';
		$( window.currentDom ).append( dom );
	})


	$('.js_select_dom').change(function(){
		var dom_num = $(this).val();
		var dom = '<div class="aw_xl_'+ dom_num +' aw_l_'+ dom_num +' aw_m_'+ dom_num +' aw_s_'+ dom_num +' aw_col edit_dom"></div>';
		$( window.currentDom ).append( dom );
		$(this).prop('selectedIndex', 0);
	})







	// Add head

	$('.js_header li').click(function(){
		var text = prompt();

		if( text ){
			var dom_content = $(this).text();
			var dom = '<'+ dom_content +' class="aw_'+ dom_content +'">'+ text +'</'+ dom_content +'>';
			$( window.currentDom ).append( dom );
		}

	})

	// Typography

	$('.js_typography li').click(function(){

		var text = prompt();

		 if( text ){		
			var dom_content = $(this).text();

			if( dom_content == 'ul' || dom_content == 'ol' ){
				var li_text = text.split(','), li_dom = '';

				$.each( li_text, function(k, v){
					li_dom += '<li>'+ v +'</li>';
				})
				var dom = '<'+ dom_content +' class="aw_list">'+ li_dom +'</'+ dom_content +'>';
				$( window.currentDom ).append( dom );
				return;

			}

			if( dom_content == 'a' ){
				var dom = '<a class="aw_a" href="#">'+ text +'</a>';
				$( window.currentDom ).append( dom );
				return;
			}

			if( dom_content == 'i' ){
				var dom = '<'+ dom_content +' class="'+ text +'"></'+ dom_content +'>';
				$( window.currentDom ).append( dom );
				return;
			}

			var dom = '<'+ dom_content +' class="aw_'+ dom_content +'">'+ text +'</'+ dom_content +'>';
			$( window.currentDom ).append( dom );

		}


	});



	//  Inputs 

	$('.js_inputs li').click(function(){
		var dom_content  = $(this).text();
		var dom= '';

		if( dom_content == 'Text' ){
			dom = '<input type="text" class="aw_input aw_block" />';
		} else if( dom_content == 'Number' ) {
			dom = '<input type="number" class="aw_input aw_block"/>';
		} else if( dom_content == 'Check Box' ){
			dom = '<span class="check_masked"><input type="checkbox" class="aw_checkbox"><i></i><em>Label</em></span>';
		} else if( dom_content == 'Radio' ){
			dom = '<span class="radio_masked"><input type="radio" class="aw_radio"><i></i><em>Label</em></span>';
		} else if( dom_content == 'Input Button' ){
			dom = '<input type="submit" class="aw_button aw_block" />';
		} else if( dom_content == 'Button' ){
			dom = '<button class="aw_button aw_block">Submit</button>';
		}

		$( window.currentDom ).append( dom );
	});




	// CSS adder 

	$('.js_add_css').click(function(){
		window.edit = false;
		$('.css_adder').show();
	});

	$('.css_adder .js_close').click(function(){
		window.edit = true;
		$('.css_adder').hide()//.find('textarea').val('');	
	});

	$('.css_adder .js_save').click(function(){
		var css_code = $('.css_adder').find('textarea').val();
		$('style').text('');
		$('style').append( css_code );
	});





	// Colors Select 

	$('.js_color .color_div').click(function(){
		$(this).css({'border': 'solid 2px #FFF'})
		var domClass = $(this).text();
		$( window.currentDom ).addClass( domClass );
	});



	// Helper Class

	// Margin All
	$('.js_mrg li').click(function(){
		var mrg_v = $(this).text();
		$( window.currentDom ).addClass( 'aw_mrg_' + mrg_v );
	})


	// MArgin Top
	$('.js_mrg_top li').click(function(){
		var mrg_v = $(this).text();
		$( window.currentDom ).addClass( 'aw_mrg_t_' + mrg_v );
	})

	// MArgin Left
	$('.js_mrg_left li').click(function(){
		var mrg_v = $(this).text();
		$( window.currentDom ).addClass( 'aw_mrg_l_' + mrg_v );
	})

	// MArgin Right
	$('.js_mrg_right li').click(function(){
		var mrg_v = $(this).text();
		$( window.currentDom ).addClass( 'aw_mrg_r_' + mrg_v );
	})

	// MArgin Right
	$('.js_mrg_bottom li').click(function(){
		var mrg_v = $(this).text();
		$( window.currentDom ).addClass( 'aw_mrg_b_' + mrg_v );
	})




	// Padding
	$('.js_pad li').click(function(){
		var pad_v = $(this).text();
		$( window.currentDom ).addClass( 'aw_pad_' + pad_v );
	})

	// Padding Top
	$('.js_pad_top li').click(function(){
		var pad_v = $(this).text();
		$( window.currentDom ).addClass( 'aw_pad_t_' + pad_v );
	})

	// Padding Left
	$('.js_pad_left li').click(function(){
		var pad_v = $(this).text();
		$( window.currentDom ).addClass( 'aw_pad_l_' + pad_v );
	})

	// Padding Right
	$('.js_pad_right li').click(function(){
		var pad_v = $(this).text();
		$( window.currentDom ).addClass( 'aw_pad_r_' + pad_v );
	})

	// Padding Bottom
	$('.js_pad_bottom li').click(function(){
		var pad_v = $(this).text();
		$( window.currentDom ).addClass( 'aw_pad_b_' + pad_v );
	})




	// Border

	$('.js_border li').click(function(){
		var brd = $(this).text();

		if( brd == 'Border none'){
			$( window.currentDom ).addClass( 'aw_b0' );
		} else if( brd == 'Border all'){
			$( window.currentDom ).addClass( 'aw_ba' );
		} else if( brd == 'Border top'){
			$( window.currentDom ).addClass( 'aw_bt' );
		} else if( brd == 'Border left'){
			$( window.currentDom ).addClass( 'aw_bl' );
		} else if( brd == 'Border right'){
			$( window.currentDom ).addClass( 'aw_br' );
		} else if( brd == 'Border bottom'){
			$( window.currentDom ).addClass( 'aw_bb' );
		}

	})



	// Radius

	$('.js_radius li').click(function(){
		var br =  $(this).text();
		$( window.currentDom ).addClass( 'aw_br' + br );
		
	})




	// Floats

	$('.js_floats li').click(function(){
		var fl = $(this).text();

		if( fl == 'Right'){
			$( window.currentDom ).addClass( 'aw_fl_right' );
		} else if( fl == 'Left'){
			$( window.currentDom ).addClass( 'aw_fl_left' );
		} else if( fl == 'Center'){
			$( window.currentDom ).addClass( 'aw_fl_center' );
		}

	})



	// Floats

	$('.js_align li').click(function(){
		var fl = $(this).text();

		if( fl == 'Right'){
			$( window.currentDom ).addClass( 'aw_al_right' );
		} else if( fl == 'Left'){
			$( window.currentDom ).addClass( 'aw_al_left' );
		} else if( fl == 'Center'){
			$( window.currentDom ).addClass( 'aw_al_center' );
		} else if( fl == 'Justify'){
			$( window.currentDom ).addClass( 'aw_al_justify' );
		}

	})




	// Fonts

	$('.js_fonts li').click(function(){
		var fnt = $(this).text();
		$( window.currentDom ).addClass( 'aw_font_'+ fnt );
	})




	// Transformation

	$('.js_trans li').click(function(){
		var tr = $(this).text();

		if( tr == 'Lowercased'){
			$( window.currentDom ).addClass( 'aw_lowercase' );
		} else if( tr == 'Uppercased'){
			$( window.currentDom ).addClass( 'aw_uppercase' );
		} else if( tr == 'Capitalized'){
			$( window.currentDom ).addClass( 'aw_capitalize' );
		}

	})



	//  Up and Down Dom

	$('.js_down_dom').on('click', function(){
		$( window.currentDom ).before( $( window.currentDom ).next() );
	});

	$('.js_up_dom').on('click', function(){
		$( window.currentDom ).after( $( window.currentDom ).prev() );
	});

	$(document).on('keyup', function(e){
		if( e.which ==  38){
			$( window.currentDom ).after( $( window.currentDom ).prev() );
		} else if( e.which == 40 ){
			$( window.currentDom ).before( $( window.currentDom ).next() );
		}
	});





	// Download layout as file

	$('.js_view_code').click(function(){
		window.edit = true;
		$('*').removeClass('edit_dom').removeClass('select_dom').removeClass('selected_dom');
		var dom = $('.aw_container');
		var doms = dom[0].outerHTML;
		var css = $('style').text();
				
		options = {
			"indent": "auto",
			"indent-spaces": 4,
			"wrap": 1000,
			"markup": true,
			"output-xml": false,
			"numeric-entities": true,
			"quote-marks": true,
			"quote-nbsp": false,
			"show-body-only": false,
			"quote-ampersand": false,
			"break-before-br": false,
			"uppercase-tags": false,
			"uppercase-attributes": false,
			"drop-font-tags": false,
			"tidy-mark": false
		}

		var doms = tidy_html5( doms , options);

		if( css ){
			$('body').append('<span class="download_popup"><a class="save_layout" download="layout.html" href="data:html/plain;charset=UTF-8,' + encodeURIComponent( doms ) + '" >Download<a/><a class="save_css" download="style.css" href="data:html/plain;charset=UTF-8,' + encodeURIComponent( css ) + '" >Download<a/></span>');
			$(".save_layout")[0].click();
			$(".save_css")[0].click();

		} else {
			$('body').append('<span class="download_popup"><a class="save_layout" download="layout.html" href="data:html/plain;charset=UTF-8,' + encodeURIComponent( doms ) + '" >Download<a/></span>');
			$(".save_layout")[0].click();
		}

		$('.download_popup').remove();

	});





	$('.js_add_class').on('click', function(){
		var add_class = prompt();
		$( window.currentDom ).addClass( add_class );
	});

	$(document).on('click', '.js_removeClass', function(){
		var dom = $(this).next().text();
		$( window.currentDom ).removeClass( dom );
	});

	function removeDom_select(){
		$('.aw_container, .aw_wrapper, .aw_row, .aw_col').removeClass('select_dom');
	}

	$(document).mouseover(function(e){

		if( window.edit == false ){ return false; }

		if( $( e.target ).hasClass('aw_container') ){
			removeDom_select();
			$(e.target).addClass('select_dom');
		}
		if( $( e.target ).hasClass('aw_wrapper') ){
			removeDom_select();
			$(e.target).addClass('select_dom');
		}
		if( $( e.target ).hasClass('aw_col') ){
			removeDom_select();
			$(e.target).addClass('select_dom');
		}
		if( $( e.target ).hasClass('aw_row') ){
			removeDom_select();
			$(e.target).addClass('select_dom');
		}

	});

	$(document).on('click', function(e){
		if( !$(e.target).hasClass('class_list') ){
			$('.class_list').hide();
		}
		if( window.edit == false ){ return false; }

		if( $(e.target).parents().hasClass('navs') ){
			return false;
		} else if ( $(e.target).hasClass('navs') ){
			return false;
		} else if ( $(e.target).parents().hasClass('css_adder') ){
			return false;
		}
		window.currentDom = $(e.target);
		$('*').removeClass('selected_dom');
		$(e.target).addClass('selected_dom');

	});


	$( document ).contextmenu(function( e ) {
	  	e.preventDefault();

	  	if( window.edit == false ){	return false; }
	  	
	  	var classie = $(e.target)[0].className.split(' ');

	  	if( classie == "" ) { return; }

	  	var domClass = '';
	  	$.each( classie, function( k, v ){
	  		if( v == 'edit_dom' || v == 'selected_dom' || v == 'select_dom' ){ return; }
	  		domClass += '<li><span class="js_removeClass">Delete</span><span>'+ v +'</span></li>'
	  	})

	  	$('.class_list .aw_list').remove();
	  	$('.class_list').append('<ul class="aw_list"></ul>');
	  	$('.class_list .aw_list').append( domClass );
	  	$('.class_list').show().css({'left': e.clientX,'top': e.clientY });

	});


	$(document).delegate('#textbox', 'keydown', function(e) { 
		var keyCode = e.keyCode || e.which; 

		if (keyCode == 9) { 
			e.preventDefault(); 
			var start = $(this).get(0).selectionStart;
			var end = $(this).get(0).selectionEnd;

			$(this).val($(this).val().substring(0, start)
			            + "\t"
			            + $(this).val().substring(end));
 
			$(this).get(0).selectionStart = 
			$(this).get(0).selectionEnd = start + 1;
		} 
	});




})


