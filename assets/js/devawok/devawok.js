
// devAwokInit() this function for Angular Usage

//devAwokInit()

function devAwokInit(){

	jQuery(document).ready(function($) {

		// Dropdown Item
		$('.aw_dropdown > button').bind( 'click', function( e ){ 
			e.preventDefault();

			var pos = 'left';
			var screen = $(document).innerWidth() / 2;

			if( $(this).offset().left >= screen ){ pos = 'right'; }

			if( $(this).next().hasClass('active') ){
				$(this).next('div').removeClass('active').css( pos ,'0px').fadeOut(123);
			} else {
				$('.aw_dropdown div').hide().removeClass('active');
				$(this).next('div').addClass('active').css( pos ,'0px').fadeIn(123);
			}


		});






		// Tabs
		$('.aw_tabs ol li').bind('click', function( e ){
			e.preventDefault();

			if( $(this).parent().parent().hasClass('nojs') ){ 
				return false;

			} else {
				$(this).parent().parent().find('ul').find('li').removeClass('active').hide();
				$(this).parent().parent().find('ol').find('li').removeClass('active');

				var ind = $(this).index();
				$(this).addClass('active');
				$(this).parent().next().find('li').eq(ind).show().addClass('active');
			}


		});




		// Accordion

		$('.aw_accordion ul li.header').bind('click', function( e ){
			e.preventDefault();

			if( $(this).next().find('div').hasClass('active') ){
				$(this).next().find('div').slideUp( 333 , function(){
					$(this).removeClass('active');	
				})
				return false;	
			}

			$(this).parent().parent().find('div').removeClass( 'active' );
			$(this).parent().parent().find('div').slideUp( 333 );

			$(this).next().find('div').slideToggle( 333 ).addClass('active');

		});





		// Tool Tip

		$(document).bind('mouseover', function( e ){

			var tooltip_top 	= $($(e.target)[0]).attr('data-tooltip-top');
			var tooltip_left 	= $($(e.target)[0]).attr('data-tooltip-left');
			var tooltip_right 	= $($(e.target)[0]).attr('data-tooltip-right');
			var tooltip_bottom 	= $($(e.target)[0]).attr('data-tooltip-bottom');

			if( tooltip_top ){
				var text = tooltip_top;
				$($(e.target)[0]).parent().append('<span class="tooltip tooltip_top">'+ text +'</span>');

				var width = $($(e.target)[0]).innerWidth();
				var height = $($(e.target)[0]).innerHeight();
				var tWidth = $('.tooltip').innerWidth();
				var tHeight = $('.tooltip').innerHeight();
				var top = $(e.target)[0].offsetTop;
				var left = $(e.target)[0].offsetLeft;

				$('.tooltip').css({
					'top': top - tHeight - 6 ,
					'left': left - tWidth /2 + width /2
				})

			}

			if (tooltip_left){
				var text = tooltip_left;
				$($(e.target)[0]).parent().append('<span class="tooltip tooltip_left">'+ text +'</span>');
				
				var width = $($(e.target)[0]).innerWidth();
				var height = $($(e.target)[0]).innerHeight();
				var tWidth = $('.tooltip').innerWidth();
				var tHeight = $('.tooltip').innerHeight();
				var top = $(e.target)[0].offsetTop;
				var left = $(e.target)[0].offsetLeft;

				$('.tooltip').css({
					'top': top + height / 2 - tHeight / 2,
					'left': left - tWidth - 5
				})

			} 

			if (tooltip_right){
				var text = tooltip_right;

				$($(e.target)[0]).parent().append('<span class="tooltip tooltip_right">'+ text +'</span>');

				var width = $($(e.target)[0]).innerWidth();
				var height = $($(e.target)[0]).innerHeight();
				var tWidth = $('.tooltip').innerWidth();
				var tHeight = $('.tooltip').innerHeight();
				var top = $(e.target)[0].offsetTop;
				var left = $(e.target)[0].offsetLeft;

				$('.tooltip').css({
					'top': top + height / 2 - tHeight / 2,
					'left': left + width + 7 
				})

			} 

			if (tooltip_bottom){
				var text = tooltip_bottom; 
				
				$($(e.target)[0]).after('<span class="tooltip tooltip_bottom">'+ text +'</span>');

				var width = $($(e.target)[0]).innerWidth();
				var height = $($(e.target)[0]).innerHeight();
				var tWidth = $('.tooltip').innerWidth();
				var top = $(e.target)[0].offsetTop
				var left = $(e.target)[0].offsetLeft

				$('.tooltip').css({
					'top': top + height + 10,
					'left': left - tWidth /2 + width /2
				})

			}

		}).bind('mouseout', function( e ){
			$('.tooltip').remove();
		})




		


		// Toggle

		$( '*' ).on('click', function( e ){

			var toggle = $($(e.target)[0]).attr('data-toggle');
			var toggleOpt = $($(e.target)[0]).attr('data-toggle-options')
			if( toggle || toggleOpt ){
				if (toggleOpt) {		
				var a = toggleOpt.split(' ');
					if( a[0] == 'fade'){
						$( '.'+toggle ).fadeToggle( a[1] );
						return false;
					} else if( a[0] == 'slide') {
						$( '.'+toggle ).slideToggle( a[1] );
						return false;
					}
				} else {
					$( '.'+toggle ).slideToggle( 333 );
				}
			}


		})

		



		// Tags

		$('.aw_tags input[type="text"]').bind( 'keydown', function( e ){
			$(this).removeClass('error');
			if( e.which  == 13 ){
				var text = $(this).val();
				var len = $(this).parent().find('li').length
				for( var i=0; i < len; i++ ){
					var current = $(this).parent().find('li').eq(i).text()
					if( current == text ){
						$(this).addClass('error');
						return false;
					}
				}
				$(this).before('<li>'+ text +'</li>');
				$(this).val('');
			}
		})

		$(document).on('dblclick', '.aw_tags li', function(){
			$(this).remove();
		})




		

		$('.aw_range').each(function(){
			var range = $(this).attr('data-from-to').split(',')
			var width = $(this).parent().innerWidth();

			// Range Limit Options
			$(this).jRange({
			    from: range[0],
			    to: range[1],
			    step: 1,
			    format: '%s',
			    width: width,
			    showLabels: true,
			    isRange : true
			});
		});



		//  Other Options
		/* $('.range-slider').jRange({
		    from: 0,
		    to: 100,
		    step: 1,
		    scale: [0,25,50,75,100],
		    format: '%s',
		    width: 300,
		    showLabels: true,
		    isRange : true
		}); */	

		// Single Side ranger
		/*$('.single-slider').jRange({
		    from: -2.0,
		    to: 2.0,
		    step: 0.5,
		    scale: [-2.0,-1.0,0.0,1.0,2.0],
		    format: '%s',
		    width: 300,
		    showLabels: true,
		    snap: true
		}); */






		// Date and Time Picker

		$.datetimepicker.setLocale('en');
		$('.aw_datetime').datetimepicker();
		$('.aw_date').datetimepicker({timepicker:false, format:'d/m/Y' });
		$('.aw_time').datetimepicker({datepicker:false, format:'H:i' });







		// Data table

		$('.aw_datatable').each(function(){
			$(this).DataTable({
				'bFilter': false,
				'bLengthChange': false
			});
		})







		// Carousel

		$(".aw_carousel").each(function(){
			var options = $(this).attr('data-carousel');
			var item, nav, loop, rewind, rtl,  drag;

			eval('var obj='+options);

			if( obj.item ){	item = obj.item; } else { item = 1;	}
			if( obj.loop == true ){	loop = true	} else { loop = false }
			if( obj.nav == true ){ nav = true } else { nav = false }
			if( obj.rewind == true ){ rewind = true	} else { rewind = false	}
			if( obj.rtl == true ){ rtl = true } else { rtl = false }
			if( obj.drag == true ){ drag = true } else { drag = false }

			$(this).owlCarousel({
				rtl 		: rtl,
				items       : item,	
		        nav         : nav,
		        smartSpeed  : 900,
		        lazyLoad 	: true,
		        loop 		: loop,
		        mouseDrag 	: drag

			}).on('changed.owl.carousel', update_pagination );

			if( rewind == true ){
				$(this).on('click', '.owl-next', function(e) {
			        if( slide_clicked != 'Y' ){ $(this).trigger('to.owl.carousel',0); } 
			        slide_clicked   = 'N';
			    });
			    $(this).on('click', '.owl-prev', function(e) {    
			        if( slide_clicked != 'Y' ){ $(this).trigger('to.owl.carousel',500); } 
			        slide_clicked   = 'N';
			    });
			}
		
			function update_pagination(){
		    	slide_clicked   = 'Y';
		    }

		    var slide_clicked   = '';

		})










		// HZoom Image Zoomer

		if( typeof is_rtl !=  'undefined' ){
	        // RTL
	        var rtl_dir                 = true;
	        var zoom_dir                = 'left';
	        window.zoom_adjust          = 48; // HOVER HZOOMER ADJUST 
	        window.h_area_dp_none       = 'display:none;'; // HIDING ZOOM POINTER BOUNDARY
	    }
	    else{
	        // OTHERS
	        var rtl_dir                 = false;
	        var zoom_dir                = 'right';            
	        window.zoom_adjust          = 0;
	        window.h_area_dp_none       = '';            
	    }

		$('.aw_hzoom').each(function(){
	        $(this).find('img').ImageZoom({
	            type: 'standard',
	            bigImageSrc: $(this).find('img').attr('data-hzoom'),
	            zoomSize: [ 600, $(this).height() + 22 ],
	            alignTo: $(this).parent().attr('id') , // 'align-zoom'
	            margin:10,
	            offset: [0, 0],
	            preload: false,
	            zoomViewerClass: 'standardViewer',
	            position: zoom_dir // for RTL
	        });


		})
		



		//  Star Ratings
		var current = 0;
		$('.aw_rating').each(function(){
			var rate = $(this).find('span').attr('data-rate')
			$(this).find('span').css('width', rate );
			$(this).on('mousemove',function(e){
				if( e.offsetX >= 5 ){
					$(this).find('span').css('width', '20%');
					current = 1
				}if( e.offsetX >= 20 ){
					$(this).find('span').css('width', '40%');
					current = 2
				}if( e.offsetX >= 35 ){
					$(this).find('span').css('width', '60%');
					current = 3
				}if( e.offsetX >= 45 ){
					$(this).find('span').css('width', '80%');
					current = 4
				}if( e.offsetX >= 60 ){
					$(this).find('span').css('width', '100%');
					current = 5
				}
			}).mouseout(function(){
				$(this).find('span').css('width', rate );
			}).click(function(){
				$(this).find('span').attr('rating', current )
			})
		})

		function aw_ratings( dom ){
			var a = $(dom).find('span').attr('rating')
			return a;
		}
		

		$('.aw_rating').click(function(){
			//rating
			var a = aw_ratings($(this))
			console.log(  a  )
			
		})














		$(document).on('click', '.aw_ajax input[type="button"]' ,function(e){
			e.stopPropagation();

			var type = $(this).parent().attr('method');
			var data = $(this).serialize();

			var self = $(this);

			$.ajax({
			    type : 'GET',
			    //dataType: "html", 
			    url  : window.location.href.split('?')[0],
			    data : {
			        data
			    },success:function( response ) { 
			    	
					//self.hide();

			        // console.log( response.find('.aw_ajax') )

			        console.log( response )


			    }

			});




		})



















		// Close all Companent

		$(document).on('click', function( e ){	

			var str = $(e.target)[0].parentElement.className;
			if( !$(e.target).parentsUntil('aw_dropdown').hasClass('aw_dropdown') ){
				$('.aw_dropdown div').hide().removeClass('active');
			} 

			if( $($(e.target)[0]).hasClass('aw_modal') || $($(e.target)[0]).hasClass('close_icon') ){
				$('.aw_modal').hide();
			}

		});


		// End jQuery

	});


}
















// Initializing all DOM Elements

function initDOM(){	
	$('.aw_dropdown div').hide().removeClass('active');
	$('.aw_modal').hide();
}









// temp use only



$(function(){

	// modal 1
	$('.js_modal').bind('click', function( e ){	
		$('.modal_one').show();

	})

	// Modal 2
	$('.js_modal2').bind('click', function( e ){
		$('.modal_two').show();

	})



})




