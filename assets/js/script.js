


$(function(){
    $(window).load(function(){
        $('textarea').each(function(){
            var height = $(this)[0].scrollHeight;
            $(this).height( height - 10 );
            $(this).attr('spellcheck',false);
        })

        $('#copyButton').click(function(){
             $(this).prev().focus().select();
             document.execCommand("copy");             
        })
    })


    $('ul li a').click(function(){
        $('ul li a').removeClass('active');
        $(this).addClass('active');
    })


    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
            }
        }
    });



    $(document).on('click', '.open_modal', function(){
        $('.modal_exp').show();
    })

    $(document).on('click', '.open_modal2', function(){
        $('.modal_exp2').show();
    })


    $('.slide_toggle').click(function(){
        $('.menu_list > li div').slideUp(333);

        $(this).next('div').slideDown(666);
    })


})


function scrolltop(){
    $('body, html').animate({ scrollTop: "0px" });

}


function jQueryInit(){
    $(function(){
        $(document).on('click', '#copyButton', function(){
             $(this).prev().focus().select();
             document.execCommand("copy");
        })     
        $('textarea').each(function(){
            var height = $(this)[0].scrollHeight;

            $(this).height( height - 10 );
            $(this).attr('spellcheck',false);
        })
    })

}


