//

var express = require('express');
var app = express();

app.use( express.static(  __dirname + '/' ) );


app.get('/layout-maker', function( req, res ){
	res.sendfile( 'template.html' );
})

app.get('/*', function( req, res ){
	res.sendfile( 'index.html' );
})


app.listen( 80 , 'localhost' );